package com.example.asd.hello2;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Messenger mService;
    private Messenger messenger=new Messenger(new Handler(){
        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(MainActivity.this,((Bundle)msg.obj).getString("name"),Toast.LENGTH_SHORT).show();
        }
    });
    private ServiceConnection serviceConnection=new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService=new Messenger(iBinder);
            Toast.makeText(MainActivity.this,"connected",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };
    private Button query;
    private EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        query=(Button)findViewById(R.id.main_query);
        editText=(EditText)findViewById(R.id.main_num);
        query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message message=new Message();
                message.arg1=Integer.parseInt(editText.getText().toString());
                message.replyTo=messenger;
                if (mService!=null){
                    try {
                        mService.send(message);
                        Toast.makeText(MainActivity.this,"sent",Toast.LENGTH_SHORT).show();
                    } catch (RemoteException e) {
                        Toast.makeText(MainActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        Intent intent=new Intent();
        intent.setAction("com.xchat.stevenzack.query");
        final Intent ei=new Intent(createFrom(this,intent));
        bindService(ei,serviceConnection,BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(serviceConnection);
    }
    public static Intent createFrom(Context context,Intent intent){
        PackageManager pm=context.getPackageManager();
        List<ResolveInfo> resolveInfos=pm.queryIntentServices(intent,0);
        if (resolveInfos==null||resolveInfos.size()!=1){
            return null;
        }
        ResolveInfo info=resolveInfos.get(0);
        String pkg=info.serviceInfo.packageName;
        String className=info.serviceInfo.name;
        ComponentName componentName=new ComponentName(pkg,className);
        Intent intent1=new Intent(intent);
        intent1.setComponent(componentName);
        return intent1;
    }
}
