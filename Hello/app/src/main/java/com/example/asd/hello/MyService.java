package com.example.asd.hello;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public class MyService extends Service {
    private final Messenger messenger=new Messenger(new IncomingHandler());
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
        // TODO: Return the communication channel to the service.
    }
    class IncomingHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            Message msgToClient=Message.obtain(msg);
            Bundle bundle=new Bundle();
            switch (msg.arg1){
                case 100:
                    bundle.putString("name", "one");
                    break;
                case 101:
                    bundle.putString("name", "two");
                    break;
                case 102:
                    bundle.putString("name", "three");
                    break;
                case 103:
                    bundle.putString("name", "four");
                    break;
                default:
                    bundle.putString("name", "others");
                    break;
            }
            msgToClient.obj=bundle;
            try {
                msg.replyTo.send(msgToClient);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
