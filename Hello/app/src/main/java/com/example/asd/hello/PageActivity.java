package com.example.asd.hello;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class PageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);
        TextView txt=(TextView)findViewById(R.id.page_txt);
        Intent intent=getIntent();
        int y=intent.getIntExtra("year",0);
        if (checkYear(y))
            txt.setText("是闰年");
        else
            txt.setText("不是闰年");
    }
    public static boolean checkYear(int y){
        if (y%4==0&&y%100!=0)
            return true;
        else
            return false;
    }
}
